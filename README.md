
This is the classic directory service

Expected Features:

a. An admin can log in and create a listing for a business

b. The admin can create as many categories as he desires

b. A business can belong to multiple categories e.g If web design and software development are two different categories, INITS would belong to the two at the same time

c. The general public (no need for registration and login) can search and view listings. Search should go through name and description

d. A listing should contain the name of the business, a description of what they do and spaces for website url, contact email, phones and address

e. The admin should be able to delete or modify a business listing

f. The admin should be able to see how many views a business listing has had. 

g. Expose the search functionality via an API as well.


Our preferred framework for PHP projects is CakePHP.

Build it using CakePHP 2.6 or 3.0. Don't bake the controller or views. Bake==auto-generate code. Write the code yourself for the controller or views. You should only generate code for the model and schema.