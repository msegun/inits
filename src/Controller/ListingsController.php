<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class ListingsController extends AppController{
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('custom_admin');
        $all_categories = TableRegistry::get('Categories')->find('all')->toArray();
        $this->set("alert", "");
        $this->set("all_categories", $all_categories);
    }
    
    public function add(){
        $this->set("page_title", "Add a new Business");
        if($this->request->is('post')){
            $bizTable = TableRegistry::get('Businesses');
            $bizEntity = $bizTable->newEntity();
            $bizEntity->set("name", strtoupper($this->request->data['business_name']));
            $bizEntity->set("description", $this->request->data['business_description']);
            $bizEntity->set("website", $this->request->data['business_website']);
            $bizEntity->set("email", $this->request->data['business_email']);
            $bizEntity->set("phone", $this->request->data['business_telephone']);
            $bizEntity->set("address", $this->request->data['business_address']);
            $array_of_categories_picked = $this->request->data['categories'];
            if($bizTable->save($bizEntity)){
                $inserted_id = $bizEntity->get("b_id");
                $this->saveToBizCategoryTable($inserted_id, $array_of_categories_picked);
                $this->set("alert", "<p class='alert alert-success'>Added Successfully</p>");
            }
            else{
                $this->set("alert", "<p class='alert alert-danger'>Business not added / Duplicate</p>");
            }
        }
        $this->set("job", "Add a new Business for Listing");
        $this->set("business_name", "");
        $this->set("business_description", "");
        $this->set("business_address", "");
        $this->set("business_email", "");
        $this->set("business_telephone", "");
        $this->set("business_website", "");
        $this->set("list_of_categories", array());
        $this->set("task", "Add");
    }
    
    
    public function view(){
        if(isset($this->request->query['delete'])){
            $idToDelete = $this->request->query['delete'];
            $bizTable = TableRegistry::get('Businesses');
            $bizTable->deleteAll(['b_id'=>$idToDelete]);
            $bizCategoryTable = TableRegistry::get('BusinessCategories');
            $bizCategoryTable->deleteAll(['business_id'=>$idToDelete]);
        }
        $this->set("page_title", "View and Manage all Business");
        $bussiness_table = TableRegistry::get('Businesses');
        $query = $bussiness_table->find('all')
                ->select(['name', 'v', 'b_id']);
        $this->set('query', $query);
    }
    
    public function edit(){
        $this->set("page_title", "Edit Business");
        $bussiness_table = TableRegistry::get('Businesses');
        $bizId = $this->request->params['id'];
        if($this->request->is('post')){
            $bizEntity = $bussiness_table->get($bizId);
            $bizEntity->set("name", strtoupper($this->request->data['business_name']));
            $bizEntity->set("description", $this->request->data['business_description']);
            $bizEntity->set("website", $this->request->data['business_website']);
            $bizEntity->set("email", $this->request->data['business_email']);
            $bizEntity->set("phone", $this->request->data['business_telephone']);
            $bizEntity->set("address", $this->request->data['business_address']);
            $array_of_categories_picked = $this->request->data['categories'];
            if($bussiness_table->save($bizEntity)){
                $bizCategoryTable = TableRegistry::get('BusinessCategories');
                $bizCategoryTable->deleteAll(['business_id'=>$bizId]);
                $this->saveToBizCategoryTable($bizId, $array_of_categories_picked);
                $this->set("alert", "<p class='alert alert-success'>Update Successfully</p>");
            }
            else{
                $this->set("alert", "<p class='alert alert-danger'>Update Filed / Duplicate</p>");
            }
        }
        
        $query = $bussiness_table->find('all')->select(['categories.c_id', 'name', 'description',
                'website', 'email', 'phone', 'address'])
                    ->leftJoin('business_categories', 'businesses.b_id = business_categories.business_id')
                    ->leftJoin('categories', 'business_categories.category_id = categories.c_id')
                ->where(['b_id'=>  $bizId]);
        $business_name = "";
        $business_description = "";
        $business_address = "";
        $business_email = "";
        $business_telephone = "";
        $business_website = "";
        $categories_ids = array();
        foreach ($query as $val){
            $business_name = empty($val->name)?$business_name:$val->name;
            $business_description = empty($val->description)?$business_description:$val->description;
            $business_address = empty($val->address)?$business_address:$val->address;
            $business_email = empty($val->email)?$business_email:$val->email;
            $business_telephone = empty($val->phone)?$business_telephone:$val->phone;
            $business_website = empty($val->website)?$business_website:$val->website;
            if($val->categories['c_id'] != null){
                array_push($categories_ids, $val->categories['c_id']);
            }
        }
        $this->set("business_name", $business_name);
        $this->set("business_description", $business_description);
        $this->set("business_address", $business_address);
        $this->set("business_email", $business_email);
        $this->set("business_telephone", $business_telephone);
        $this->set("business_website", $business_website);
        $this->set("list_of_categories", $categories_ids);
        $this->set("job", "Edit Listing of : ".$business_name);
        $this->set("task", "Update");
        $this->viewBuilder()->template('add');
    }
    
    private function saveToBizCategoryTable($id, $array_picked){
        $bizCategoryTable = TableRegistry::get('BusinessCategories');
        foreach($array_picked as $cat_picked){
            $ent = $bizCategoryTable->newEntity();
            $ent->set("category_id", intval($cat_picked));
            $ent->set("business_id", $id);
            $bizCategoryTable->save($ent);
        }
    }
}