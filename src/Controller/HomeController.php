<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

class HomeController extends AppController{
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('custom'); 
    }
    
    public function index(){
        $this->set('page_title', "Welcome to Search Directory INC");
        $queryEvent = false;
        if(isset($this->request->query['search_for'])){
            $queryEvent = true;
            $keyword = $this->request->query['search_for'];
            $bussiness_table = TableRegistry::get('Businesses');
            $query = $bussiness_table->find('all')
                    ->where(["Businesses.name LIKE "=>"%".$keyword."%"])
                ->orWhere(["Businesses.description LIKE "=>"%".$keyword."%"]);
            $this->set('query', $query);
            foreach($query as $biz){
                $id = $biz->b_id;
                $increament_view = intval($biz->v) + 1;
                $newEntity = $bussiness_table->get($id);
                $newEntity->set('v', $increament_view);
                $bussiness_table->save($newEntity);
            }
        }
        $this->set('isQuery', $queryEvent);
    }
}

