<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;

class AdminController extends AppController{
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('custom_admin'); 
    }
    
    public function index(){
        $this->set("page_title", "Quick Admin Page");
        $categoriesCount = TableRegistry::get('Categories')->find('all')->count();
        $businessCount = TableRegistry::get('Businesses')->find('all')->count();
        
        $this->set("total_categories", $categoriesCount);
        $this->set("total_businesses", $businessCount);
    }
    
}
