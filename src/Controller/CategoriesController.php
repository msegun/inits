<?php
namespace App\Controller;
use Cake\ORM\TableRegistry;

class CategoriesController extends AppController{
    
    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->layout('custom_admin');
        $this->set("alert", "");
    }
    
    public function add(){
        $this->set("page_title", "Add a new Category");
        if($this->request->is('post')){
            $catTable = TableRegistry::get('Categories');
            $catEntity = $catTable->newEntity();
            $catEntity->set("category", $this->request->data['new_category']);
            if($catTable->save($catEntity)){
                $this->set("alert", "<p class='alert alert-success'>Added Successfully</p>");
            }
            else{
                $this->set("alert", "<p class='alert alert-danger'>Category not added / Duplicate</p>");
            }
        }
    }
    
}