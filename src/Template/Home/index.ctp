<div class="panel panel-primary text-center">
    <div>Search Directory Below</div>
    <div class="panel-body">
        <form>
            <input type="text" name="search_for" class="form-control"
                   placeholder="Search for business by name and description......" />
        </form>
    </div>
    
    <?php
    if($isQuery){
    ?>
        <div class="panel panel-primary" style="margin:22px;">
            <div class="panel-body">
                <?php if(empty($query)){ echo "<p class='alert alert-warning'>No Result</p>"; }else{?>
                <?php
                    foreach($query as $result){
                ?>
                    <div class="panel panel-success">
                        <div class="panel-body text-left">
                            <p>Name of Business : <em><?= $result->name ?></em></p>
                            <p>Description : <em><?= $result->description ?></em></p>
                            <p>Website : <em><?= $result->website ?></em></p>
                            <p>Address : <em><?= $result->address ?></em></p>
                            <p>Telephone : <em><?= $result->phone ?></em></p>
                        </div>
                    </div>
                    <hr>
                <?php
                    }
                ?>
                <?php } ?>
            </div>
        </div>
    <?php
    }
    ?>
    
    <a href="/">Home</a>&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp;<a href="/admin">Open Admin Page</a>
</div>
