<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

//$this->layout = false;
//
//if (!Configure::read('debug')):
//    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
//endif;
//
//$cakeDescription = 'CakePHP: the rapid development PHP framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= h($page_title) ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
</head>
<body class="home" style="margin-bottom: 80px">
    <div id="content">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="row">
                    <h2 class="text-center text-success">Welcome Administrator</h2>
                    <div class="col-sm-4">
                        <ul>
                            <li><a href="/">Homepage</a></li>
                            <li><a href="/admin">Admin Dashboard</a></li>
                            <li><a href="/admin/categories/add">Add Categories</a></li>
                            <li><a href="/admin/listings/add">Add Businesses</a></li>
                            <li><a href="/admin/listings/view">View Businesses</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-8">
                        <?= $this->fetch('content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
