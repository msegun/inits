<h3><?= $job ?></h3>
<?= $alert; ?>
<form method="post" class="col-sm-9">
    <div class="form-group">
        <input type="text" name="business_name" class="form-control" 
               value="<?= $business_name ?>" placeholder="Enter business name" required=""/>
    </div>
    <div class="form-group">
        <textarea class="form-control" name="business_address" 
                  placeholder="Enter business full address" required=""><?= $business_address ?></textarea>
    </div>
    <div class="form-group">
        <input type="text" name="business_email" class="form-control" 
                value="<?= $business_email ?>" placeholder="Enter business email" required=""/>
    </div>
    <div class="form-group">
        <input type="text" name="business_telephone" class="form-control" 
               value="<?= $business_telephone ?>" placeholder="Enter business telephone" required=""/>
    </div>
    <div class="form-group">
        <input type="text" name="business_website" class="form-control" 
               value="<?= $business_website ?>" placeholder="Enter business website" required=""/>
    </div>
    <div class="form-group">
        <textarea class="form-control" name="business_description" 
                  placeholder="Enter business description" required=""><?= $business_description ?></textarea>
    </div>
    <div class="form-group">
        <label>Select Categories Business Belongs to:</label><br />
        <?php
            foreach($all_categories as $cat){
                $checked = "";
                if(in_array($cat->c_id, $list_of_categories)){
                    $checked = "checked";
                }
        ?>
                <div class="checkbox">
                    <label>
                        <input type='checkbox' name='categories[]' value='<?= $cat->c_id ?>' <?= $checked ?>>
                        <?= $cat->category ?>
                        <i class="input-helper"></i>
                    </label>
                </div>
        <?php
            }
        ?>
    </div>
    
    <input type="submit" name="add_cat" class="btn btn-primary" value="<?= $task ?> Business" />
</form>