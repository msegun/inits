<h3>View Listings</h3>
<?= $alert; ?>
<table class="col-sm-12">
    <thead>
    <th>S/N</th>
    <th>Business Name</th>
    <th>View Counts</th>
    <th>Edit</th>
    <th>Delete</th>
    </thead>
    <tbody>
        <?php 
        $sn = 1;
            foreach($query as $one){
        ?>
        <tr>
            <td><?= $sn ?></td>
            <td><?= $one->name ?></td>
            <td><?= $one->v ?></td>
            <td><a href="/admin/listings/edit/<?= $one->b_id ?>">edit</a></td>
            <td><a class="text-danger" href="/admin/listings/view/?delete=<?= $one->b_id ?>">delete</a></td>
        </tr>
        <?php
        $sn++;
            }
        ?>
    </tbody>
</table>