<?php
namespace App\Model\Table;

use App\Model\Entity\BusinessCategory;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BusinessCategories Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Bcs
 * @property \Cake\ORM\Association\BelongsTo $Businesses
 * @property \Cake\ORM\Association\BelongsTo $Categories
 */
class BusinessCategoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('business_categories');
        $this->displayField('bc_id');
        $this->primaryKey('bc_id');

//        $this->belongsTo('Bcs', [
//            'foreignKey' => 'bc_id',
//            'joinType' => 'INNER'
//        ]);
        $this->belongsTo('Businesses', [
            'foreignKey' => 'business_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['bc_id'], 'Bcs'));
        $rules->add($rules->existsIn(['business_id'], 'Businesses'));
        $rules->add($rules->existsIn(['category_id'], 'Categories'));
        return $rules;
    }
}
