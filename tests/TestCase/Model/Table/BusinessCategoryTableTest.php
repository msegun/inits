<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BusinessCategoryTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BusinessCategoryTable Test Case
 */
class BusinessCategoryTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BusinessCategoryTable
     */
    public $BusinessCategory;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.business_category',
        'app.buses',
        'app.cats'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BusinessCategory') ? [] : ['className' => 'App\Model\Table\BusinessCategoryTable'];
        $this->BusinessCategory = TableRegistry::get('BusinessCategory', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BusinessCategory);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
